from microbit import *

def main():
    uart.init(buadrate)
    while True:
        msg = uart.read()
        if (msg != None):
            display.show(str(msg, 'UTF-8'))
            print(str(msg, 'UTF-8'))
        else:
            display.clear()

if __name__ == "__main__":
    main()
